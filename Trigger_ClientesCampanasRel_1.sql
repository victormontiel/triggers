USE [ARista]
GO

/****** Object:  Trigger [dbo].[ClientesCampanasRel_AFTER_INSERT]    Script Date: 3/4/2019 2:37:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[ClientesCampanasRel_AFTER_INSERT]
   ON [dbo].[ClientesCampanasRel]
    AFTER INSERT
      AS
    declare @newIdCampana numeric(20,0), @nclientes int, @respPos int, @score decimal(4, 2)
    begin
        set @newIdCampana = (SELECT IdCampana from inserted)
        set @nclientes = (SELECT COUNT(*) FROM dbo.ClientesCampanasRel where ClientesCampanasRel.IdCampana = @newIdCampana)
        set @respPos = (SELECT COUNT(*) FROM dbo.ClientesCampanasRel where ClientesCampanasRel.IdCampana = @newIdCampana and ClientesCampanasRel.IdRespuestaPositiva = 1)

        if @nclientes < 1
            SET @score = 0
        ELSE
            IF @nclientes <> @respPos
				SET @score = cast(ROUND((100 * (cast(@respPos as decimal(20,4))/cast(@nclientes as decimal(20,4)))), 2) as decimal (4,2))
			ELSE
				SET @score = 99.99
				
        
        Update dbo.Campanas
            set
                NumeroClientesAsignados = @nclientes,
                NumeroRespuestasPositivas = @respPos,
                ResultadoCampana =  @score
            Where Campanas.IdCampana = @newIdCampana
    end


GO


